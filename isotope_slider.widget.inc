<?php

/**
 * Implements hook_field_widget_info().
 */
function isotope_slider_field_widget_info() {
  $widgets['isotope_slider_puzzles'] = array(
    'label' => t('Isotope Slider Puzzles'),
    'description' => t('Grid with puzzles.'),
    'field types' => array('entityreference'),
    'settings' => array(),
    'behaviors' => array(
      'multiple values' => FIELD_BEHAVIOR_CUSTOM,
    ),
  );
  return $widgets;
}

/**
 * Implements hook_field_widget_form().
 */
function isotope_slider_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $widget = $instance['widget'];
  if ($widget['type'] == 'isotope_slider_puzzles') {
    $target_ids = array();
    foreach ($items as $item) {
      $target_ids[] = $item['target_id'];
    }
    // Referenced entities.
    $referenced_entities_raw_distinct = entity_load($field['settings']['target_type'], $target_ids);
    // For repeatable taget entities.
    $referenced_entities_raw = $target_ids;
    foreach ($referenced_entities_raw as &$target) {
      if (isset($referenced_entities_raw_distinct[$target])) {
        $target = $referenced_entities_raw_distinct[$target];
      } else {
        unset($target);
      }
    }
    $referenced_entities = _isotope_slider_puzzles_prepare_entities($referenced_entities_raw, $field['settings']['target_type']);
    // All entities.
    $all_entities_raw = array();
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', $field['settings']['target_type']);
    // If bundle filter is set let's apply it.
    if (isset($field['settings']['handler_settings']['target_bundles'])) {
      $bundles = array();
      foreach($field['settings']['handler_settings']['target_bundles'] as $bundle) {
        $bundles[] = $bundle;
      }
      $query->entityCondition('bundle', $bundles, "IN");
    }
    $result = $query->execute();
    if (isset($result[$field['settings']['target_type']])) {
      $ids = array_keys($result[$field['settings']['target_type']]);
      $all_entities_raw = entity_load($field['settings']['target_type'], $ids);
    }
    $all_entities = _isotope_slider_puzzles_prepare_entities($all_entities_raw, $field['settings']['target_type']);
    libraries_load("isotope");
    libraries_load("packery-mode");
    $element += array(
      '#type' => 'hidden',
      '#maxlength' => 1024,
      '#attributes' => array(
        'class' => array('isotope-slider-puzzles-textfield'),
      ),
      '#default_value' => implode(',', $target_ids),
      '#element_validate' => array('_isotope_slider_puzzles_validate'),
      '#attached' => array(
        'js' => array(drupal_get_path("module", "isotope_slider"). "/js/isotope_slider_puzzles_widget.js"),
        'css' => array(drupal_get_path("module", "isotope_slider"). "/css/isotope_slider_puzzles_widget.css"),
      ),
      '#suffix' => theme("puzzles_widget", array(
        'referenced_entities' => $referenced_entities,
        'all_entities' => $all_entities,
      )),
    );
    return $element;
  }
}

function _isotope_slider_puzzles_validate($element, &$form_state, $form) {
  $value = array();
  if (!empty($element['#value'])) {
    $ids = explode(",", $element['#value']);
    foreach($ids as $id) {
      $value[] = array(
        'target_id' => $id,
      );
    }
  }
  form_set_value($element, $value, $form_state);
}

function _isotope_slider_puzzles_prepare_entities($entities, $entity_type) {
  $prepared_entities = array();
  foreach($entities as $id => $entity) {
    $entity_wrapper = entity_metadata_wrapper($entity_type, $entity);
    $prepared_entities[] = array(
      'id' => $entity_wrapper->getIdentifier(),
      'title' => $entity_wrapper->label(),
      'grid_width' => $entity_wrapper->field_isotope_puzzle_width->value(),
      'grid_height' => $entity_wrapper->field_isotope_puzzle_height->value(),
    );
  }
  return $prepared_entities;
}
<?php

/**
 * Implements hook_views_slideshow_option_definition.
 */
function isotope_slider_views_slideshow_option_definition() {
  $options['isotope_slider'] = array(
    'use_isotope_slider' => array('default' => 0),
    'isotope_slider_settings' => array(
      'grid_width' => array('default' => 0),
      'grid_height' => array('default' => 0),
    ),
  );
  return $options;
}

/**
 * Implements hook_views_slideshow_slideshow_slideshow_type_form().
 */
function isotope_slider_views_slideshow_slideshow_type_form(&$form, &$form_state, &$view) {
  ctools_include('dependent');
  // Load a list of "isotope-extended" views_slideshows.
  $available_carousels = array();
  foreach (module_implements('isotope_slider_slideshow_implementation_info') as $module) {
    $function = $module . '_isotope_slider_slideshow_implementation_info';
    $item = call_user_func_array($function, array());
    $available_carousels[] = $item['slideshow_type'];
  }
  $form['isotope_slider'] = array(
    '#title' => t("Isotope Slider"),
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#process' => array('ctools_dependent_process'),
    '#dependency' => array('edit-style-options-slideshow-type' => $available_carousels),
    'use_isotope_slider' => array(
      '#type' => 'checkbox',
      '#title' => t("Use Isotope Slider"),
      '#description' => t("Selected gallery library should be implemented."),
      '#default_value' => $view->options['isotope_slider']['use_isotope_slider'] ? $view->options['isotope_slider']['use_isotope_slider'] : FALSE,
    ),
    'isotope_slider_settings' => array(
      '#title' => t('Isotope Slider Settings'),
      '#type' => 'fieldset',
      '#process' => array('ctools_dependent_process'),
      '#dependency' => array('edit-style-options-isotope-slider-use-isotope-slider' => array('1')),
      'grid_width' => array(
        '#title' => t("Grid width"),
        '#type' => 'textfield',
        "#default_value" => intval($view->options['isotope_slider']['isotope_slider_settings']['grid_width']),
      ),
      'grid_height' => array(
        '#title' => t("Grid height"),
        '#type' => 'textfield',
        "#default_value" => intval($view->options['isotope_slider']['isotope_slider_settings']['grid_height']),
      ),
    ),
  );
}

/*
 * Implements hook_views_slideshow_options_form_validate().
 */
function isotope_slider_views_slideshow_options_form_validate(&$form, &$form_state, &$view) {
  if ($form_state['values']['style_options']['isotope_slider']['use_isotope_slider']) {
    $error = TRUE;
    // If user wants to use isotope slider we have to check if isotope gallery implementation for chosen slideshow type exists.
    foreach (module_implements('isotope_slider_slideshow_implementation_info') as $module) {
      $function = $module . '_isotope_slider_slideshow_implementation_info';
      $item = call_user_func_array($function, array());
      if ($item['slideshow_type'] == $form_state['values']['style_options']['slideshow_type']) {
        $error = FALSE;
      }
    }
    if ($error) {
      // Actually user can't check "Use isotope slider" if it wasn't implemented for chosen views_slideshow.
      $form_state['values']['style_options']['isotope_slider']['use_isotope_slider'] = 0;
      return true;
    }
    // If grid width/height are not positive integers return error.
    $width = $form_state['values']['style_options']['isotope_slider']['isotope_slider_settings']['grid_width'];
    $height = $form_state['values']['style_options']['isotope_slider']['isotope_slider_settings']['grid_height'];
    if ($width === "" || $width <= 0) {
      form_set_error("style_options][isotope_slider][isotope_slider_settings][grid_width", t("Grid width is required and should be positive integer as long as you use isotope slider."));
      return FALSE;
    }
    if ($height === "" || $height <= 0) {
      form_set_error('style_options][isotope_slider][isotope_slider_settings][grid_height', t("Grid height is required and should be positive integer as long as you use isotope slider."));
      return FALSE;
    }
  }
}

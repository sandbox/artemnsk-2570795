<?php

/*
 * Used to add new isotope slider for views_slideshow gallery.
 */
function hook_isotope_slider_slideshow_implementation_info() {
  $item = array(
    // Name of views_slideshow gallery that will be implemented.
    'slideshow_type' => 'custom_slideshow_type',
    // Callback used to add JS before gallery callback added.
    'pre_render' => 'pre_render_callback',
    // Callback used to add JS after gallery callback added.
    'post_render' => 'post_render_callback',
  );
  return $item;
}
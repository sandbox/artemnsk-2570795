Drupal.behaviors.IsotopeSliderPuzzlesWidget = {
  attach: function (context, settings) {
    (function ($) {
      var grid_wrapper_selector = ".grid-wrapper";
      var grid_selector = grid_wrapper_selector + " .grid";
      // Function to apply proper grid size basing on grid wrapper.
      var applyGridSize = function () {
        var grid_wrapper = $(grid_wrapper_selector);
        var grid = $(grid_selector);
        var grid_width_digits = parseInt($('input[name="grid-width"]').val());
        if (!grid_width_digits) {
          // Default grid digits width on init.
          grid_width_digits = 7;
        }
        var wrapper_width_px = grid_wrapper.width();
        var grid_width_px = Math.ceil(wrapper_width_px * 0.8);
        var digit_px = Math.floor(grid_width_px / grid_width_digits);
        // Apply new grid width.
        grid.css('width', grid_width_px + "px");
        grid.css('height', parseInt($('input[name="grid-height"]').val()) * digit_px + "px");
        // Change width/height of each puzzle used inside grid.
        grid.find(".puzzle").each(function () {
          var puzzle_width_digits = parseInt($(this).attr("grid-width"));
          var puzzle_height_digits = parseInt($(this).attr("grid-height"));
          var puzzle_width_px = puzzle_width_digits * digit_px;
          var puzzle_height_px = puzzle_height_digits * digit_px;
          $(this).css("width", puzzle_width_px + "px");
          $(this).css("height", puzzle_height_px + "px");
        });
      };
      // Function to apply isotope to grid.
      var applyIsotope = function () {
        var grid = $(grid_selector);
        grid.isotope({
          itemSelector: ".puzzle",
          layoutMode: 'packery',
          transitionDuration: 0
        });
      };
      // Function to calculate input value.
      var calculateInputValue = function () {
        var values = [];
        $(grid_selector).find(".puzzle").each(function () {
          values.push($(this).attr("drupal-id"));
        });
        // TODO: pass drupal id?
        $("input.isotope-slider-puzzles-textfield").val(values.join(","));
      };
      // Function to build puzzle.
      var build_puzzle = function (width, height, title, id) {
        return '<div class="puzzle"' +
          'grid-width="' + width + '"' +
          'grid-height="' + height + '"' +
          'drupal-id="' + id + '">' +
          '<div class="title">' + title + '</div>' +
          '<div class="number"></div>' +
          '<div class="remove">X</div>' +
          '<div class="buttons">' +
          '<a class="move-left"> < </a>' +
          '<a class="move-right"> > </a>' +
          '</div>' +
          '</div>';
      };
      // Set weight of puzzle basing on position in HTML.
      var setWeights = function () {
        var grid = $(grid_selector);
        grid.find(".puzzle").each(function (i) {
          $(this).find(".number").html(i);
        });
      };
      // Returns random hex code for color.
      var getRandomColor = function () {
        var letters = '0123456789ABCDEF'.split('');
        var color = '#';
        for (var i = 0; i < 6; i++) {
          color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
      };
      // Applies random color to element.
      var applyRandomColor = function (elm) {
        elm.css('background-color', getRandomColor());
      };
      // On init we have to apply random colors to existing puzzles.
      $(grid_selector).find(".puzzle").each(function () {
        applyRandomColor($(this));
      });
      applyGridSize();
      applyIsotope();
      setWeights();
      $(".apply-grid").click(function (e) {
        applyGridSize();
        applyIsotope();
      });
      $(".add-puzzle").click(function () {
        var grid = $(grid_selector);
        var puzzle_width_digits = parseInt($(this).attr("grid-width"));
        var puzzle_height_digits = parseInt($(this).attr("grid-height"));
        var title = $(this).attr("title");
        var drupal_id = $(this).attr("drupal-id");
        var new_puzzle = $(build_puzzle(puzzle_width_digits, puzzle_height_digits, title, drupal_id));
        applyRandomColor(new_puzzle);
        grid.append(new_puzzle);
        applyGridSize();
        grid.isotope('reloadItems');
        applyIsotope();
        setWeights();
        calculateInputValue();
      });
      // Move item left inside the grid.
      $(grid_selector).delegate(".move-left", "click", function () {
        var grid = $(grid_selector);
        var current_puzzle = $(this).closest(".puzzle");
        var prev;
        var found = false;
        grid.find(".puzzle").each(function () {
          if (!found) {
            if ($(this)[0] === current_puzzle[0] && prev != undefined) {
              prev.insertAfter($(this));
              found = true;
            }
            else {
              prev = $(this);
            }
          }
        });
        grid.isotope('reloadItems');
        applyIsotope();
        setWeights();
        calculateInputValue();
      });
      // Move item right inside the grid.
      $(grid_selector).delegate(".move-right", "click", function () {
        var grid = $(grid_selector);
        var current_puzzle = $(this).closest(".puzzle");
        var prev;
        var found = false;
        grid.find(".puzzle").each(function () {
          if (!found) {
            if (prev != undefined && prev[0] === current_puzzle[0]) {
              $(this).insertBefore(prev);
              found = true;
            }
            else {
              prev = $(this);
            }
          }
        });
        grid.isotope('reloadItems');
        applyIsotope();
        setWeights();
        calculateInputValue();
      });
      // Remove puzzle button.
      $(grid_selector).delegate(".remove", "click", function () {
        var grid = $(grid_selector);
        var current_puzzle = $(this).closest(".puzzle");
        current_puzzle.remove();
        grid.isotope('reloadItems');
        applyIsotope();
        setWeights();
        calculateInputValue();
      });
    })(jQuery);
  }
};
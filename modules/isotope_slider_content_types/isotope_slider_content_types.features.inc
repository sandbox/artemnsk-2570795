<?php
/**
 * @file
 * isotope_slider_content_types.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function isotope_slider_content_types_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function isotope_slider_content_types_node_info() {
  $items = array(
    'isotope_slider_puzzle' => array(
      'name' => t('Isotope Slider Puzzle'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'isotope_slider_slide' => array(
      'name' => t('Isotope Slider Slide'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

<?php
/**
 * @file
 * isotope_slider_cycle_example.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function isotope_slider_cycle_example_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'uid' => 1,
  'title' => 'Puzzle 1x1 Drupal',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => '5e2e067f-5c7b-4a86-8cbb-69ddc6b0f70a',
  'type' => 'isotope_slider_puzzle',
  'language' => 'und',
  'created' => 1442645695,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '43e76840-d8c5-4871-93e4-a923832419ed',
  'revision_uid' => 0,
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<div style="width: 100%; height: 100%; overflow: hidden; border-radius: 10px; background: none;"><img style="width: 100%; height: 100%; margin: 0 auto; display: block;" src="https://www.drupal.org/files/druplicon_logo_2013_2_0.jpg" /></div>',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<div style="width: 100%; height: 100%; overflow: hidden; border-radius: 10px; background: none;"><img style="width: 100%; height: 100%; margin: 0 auto; display: block;" src="https://www.drupal.org/files/druplicon_logo_2013_2_0.jpg" /></div>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_isotope_puzzle_height' => array(
    'und' => array(
      0 => array(
        'value' => 1,
      ),
    ),
  ),
  'field_isotope_puzzle_width' => array(
    'und' => array(
      0 => array(
        'value' => 1,
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 1,
  'comment_count' => 0,
  'name' => 'admin',
  'picture' => 0,
  'data' => 'b:0;',
  'date' => '2015-09-19 06:54:55 +0000',
);
  $nodes[] = array(
  'uid' => 1,
  'title' => 'Puzzle 3x3 drupal',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => 'c9f77bfc-613f-47f6-8893-9ed9b5e0d407',
  'type' => 'isotope_slider_puzzle',
  'language' => 'und',
  'created' => 1442644874,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '5c77c0e7-f230-4f76-b320-7f22ce01ecea',
  'revision_uid' => 0,
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<div style="width: 100%; height: 100%; overflow: hidden; background: none;"><img style="width: auto; height: 100%; margin: 0 auto; display: block;" src="https://www.drupal.org/files/druplicon.large_.png" /></div>',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<div style="width: 100%; height: 100%; overflow: hidden; background: none;"><img style="width: auto; height: 100%; margin: 0 auto; display: block;" src="https://www.drupal.org/files/druplicon.large_.png" /></div>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_isotope_puzzle_height' => array(
    'und' => array(
      0 => array(
        'value' => 3,
      ),
    ),
  ),
  'field_isotope_puzzle_width' => array(
    'und' => array(
      0 => array(
        'value' => 3,
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 1,
  'comment_count' => 0,
  'name' => 'admin',
  'picture' => 0,
  'data' => 'b:0;',
  'date' => '2015-09-19 06:41:14 +0000',
);
  $nodes[] = array(
  'uid' => 1,
  'title' => 'Slide 2',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => 'b16c3c22-871b-4545-86ea-f2c96f4b6962',
  'type' => 'isotope_slider_slide',
  'language' => 'und',
  'created' => 1442588884,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '8cf8f59a-0757-44a7-8a9b-d86f22da64e8',
  'revision_uid' => 0,
  'body' => array(),
  'field_isotope_slider_puzzles' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 1,
  'comment_count' => 0,
  'name' => 'admin',
  'picture' => 0,
  'data' => 'b:0;',
  'date' => '2015-09-18 15:08:04 +0000',
);
  $nodes[] = array(
  'uid' => 1,
  'title' => 'Puzzle 1x1 empty',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => 'ac88d5c0-e1f2-4159-99dc-83b988ca10bc',
  'type' => 'isotope_slider_puzzle',
  'language' => 'und',
  'created' => 1442644297,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '9eb1a36d-9ad8-4790-b3f9-8765cd385030',
  'revision_uid' => 0,
  'body' => array(),
  'field_isotope_puzzle_height' => array(
    'und' => array(
      0 => array(
        'value' => 1,
      ),
    ),
  ),
  'field_isotope_puzzle_width' => array(
    'und' => array(
      0 => array(
        'value' => 1,
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 1,
  'comment_count' => 0,
  'name' => 'admin',
  'picture' => 0,
  'data' => 'b:0;',
  'date' => '2015-09-19 06:31:37 +0000',
);
  $nodes[] = array(
  'uid' => 1,
  'title' => 'Puzzle 2x1 Drupal',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => 'c12afd74-c532-4d72-9ea3-4871d392afde',
  'type' => 'isotope_slider_puzzle',
  'language' => 'und',
  'created' => 1442645331,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => 'af3f83d5-4ab6-4385-a405-b3137f8999b8',
  'revision_uid' => 0,
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<div style="width: 100%; height: 100%; overflow: hidden; border-radius: 10px; border: 1px solid CornflowerBlue; background: none;"><img style="width: 100%; padding-top: 6px; height: auto; margin: 0 auto; display: block;" src="https://www.drupal.org/files/drupal_logo-blue.png" /></div>',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<div style="width: 100%; height: 100%; overflow: hidden; border-radius: 10px; border: 1px solid CornflowerBlue; background: none;"><img style="width: 100%; padding-top: 6px; height: auto; margin: 0 auto; display: block;" src="https://www.drupal.org/files/drupal_logo-blue.png" /></div>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_isotope_puzzle_height' => array(
    'und' => array(
      0 => array(
        'value' => 1,
      ),
    ),
  ),
  'field_isotope_puzzle_width' => array(
    'und' => array(
      0 => array(
        'value' => 2,
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 1,
  'comment_count' => 0,
  'name' => 'admin',
  'picture' => 0,
  'data' => 'b:0;',
  'date' => '2015-09-19 06:48:51 +0000',
);
  $nodes[] = array(
  'uid' => 1,
  'title' => 'Puzzle 3x2 Description',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => '6d282bcd-d2c8-46b7-aa78-6f370d86f439',
  'type' => 'isotope_slider_puzzle',
  'language' => 'und',
  'created' => 1442588512,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => 'ba7939bf-bc21-44c2-be7c-f404d733541b',
  'revision_uid' => 0,
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<div style="width: 100%; height: 100%; overflow: hidden; border-radius: 10px; background: none;"><div style="height: 100%; padding: 10px; background-color: CornflowerBlue; color: white; font-size: 20px;">
All these items are Puzzle nodes placed by Isotope using packery strategy. You can achieve empty spaces using empty puzzles.
</div></div>',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<div style="width: 100%; height: 100%; overflow: hidden; border-radius: 10px; background: none;">
<div style="height: 100%; padding: 10px; background-color: CornflowerBlue; color: white; font-size: 20px;">
All these items are Puzzle nodes placed by Isotope using packery strategy. You can achieve empty spaces using empty puzzles.
</div>
</div>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_isotope_puzzle_height' => array(
    'und' => array(
      0 => array(
        'value' => 2,
      ),
    ),
  ),
  'field_isotope_puzzle_width' => array(
    'und' => array(
      0 => array(
        'value' => 3,
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 1,
  'comment_count' => 0,
  'name' => 'admin',
  'picture' => 0,
  'data' => 'b:0;',
  'date' => '2015-09-18 15:01:52 +0000',
);
  $nodes[] = array(
  'uid' => 1,
  'title' => 'Puzzle 2x2 Drupal 8',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => 'f611631e-a295-4f19-972a-8d304f3be99d',
  'type' => 'isotope_slider_puzzle',
  'language' => 'und',
  'created' => 1442646025,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => 'e37c3e6e-e0f9-4fe6-a6b0-a41cf2d4d045',
  'revision_uid' => 0,
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<div style="width: 100%; height: 100%; overflow: hidden; border-radius: 10px; background: none;"><img style="width: auto; height: 100%; margin: 0 auto; display: block;" src="https://www.drupal.org/files/drupal%208%20logo%20isolated%20CMYK%2072.png" /></div>',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<div style="width: 100%; height: 100%; overflow: hidden; border-radius: 10px; background: none;"><img style="width: auto; height: 100%; margin: 0 auto; display: block;" src="https://www.drupal.org/files/drupal%208%20logo%20isolated%20CMYK%2072.png" /></div>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_isotope_puzzle_height' => array(
    'und' => array(
      0 => array(
        'value' => 2,
      ),
    ),
  ),
  'field_isotope_puzzle_width' => array(
    'und' => array(
      0 => array(
        'value' => 2,
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 1,
  'comment_count' => 0,
  'name' => 'admin',
  'picture' => 0,
  'data' => 'b:0;',
  'date' => '2015-09-19 07:00:25 +0000',
);
  $nodes[] = array(
  'uid' => 1,
  'title' => 'Slide 1',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => '46e59b38-cda8-46ed-a5aa-72cebc62216f',
  'type' => 'isotope_slider_slide',
  'language' => 'und',
  'created' => 1442588812,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => 'ec4c03b7-e740-4f7f-b7e9-0c08899f2047',
  'revision_uid' => 0,
  'body' => array(),
  'field_isotope_slider_puzzles' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 1,
  'comment_count' => 0,
  'name' => 'admin',
  'picture' => 0,
  'data' => 'b:0;',
  'date' => '2015-09-18 15:06:52 +0000',
);
  return $nodes;
}

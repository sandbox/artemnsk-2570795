<?php
/**
 * @file
 * isotope_slider_cycle_example.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function isotope_slider_cycle_example_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function isotope_slider_cycle_example_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

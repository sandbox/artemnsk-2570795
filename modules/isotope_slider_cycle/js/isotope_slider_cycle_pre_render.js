Drupal.behaviors.IsotopeSliderCyclePreRender = {
  attach: function (context, settings) {
    (function ($) {
      // Apply Isotope.
      var applyIsotope = function () {
        $(isotopeSelector).isotope({
          itemSelector: itemSelector,
          layoutMode: 'packery',
          transitionDuration: 0
        });
      };
      var itemSelector = ".node-isotope-slider-puzzle";
      var isotopeSelector = ".node-isotope-slider-slide";
      var grid_width = parseInt(settings.isotope_slider.grid_width);
      var grid_height = parseInt(settings.isotope_slider.grid_height);
      // Used to set a width;
      var width_selector = $(".views-slideshow-cycle-main-frame");
      if (width_selector) {
        // Set Isotope wrapper and Isotope elements height depends on browser width and gallery grid width.
        var setSize = function () {
          // Trick: makes slideshow elements visible for a while to successfully apply isotope.
          $(".views_slideshow_cycle_hidden").css("display", "block");
          // Set each slide width to desired region.
          var container_width = $(width_selector).width();
          var step_size = container_width / grid_width;
          $(isotopeSelector).css("width", container_width + "px");
          // Set Isotope wrapper height.
          $(isotopeSelector).css("height", Math.ceil(step_size * grid_height) + "px");
          // Set Isotope elements height.
          for (var i = 0; i < $(itemSelector).length; i++) {

          }
          $(itemSelector).each(function () {
            // Retrieve puzzle width and height.
            var puzzle_width = parseInt($(this).attr("isotope-slider-width"));
            var puzzle_height = parseInt($(this).attr("isotope-slider-height"));
            $(this).css("width", puzzle_width / grid_width * 100 + "%");
            $(this).css("height", Math.floor(step_size * puzzle_height) + "px");
          });
          applyIsotope();
          $(".views_slideshow_cycle_hidden").css("display", "none");
        };
        // Set right height on init and screen resize.
        setSize();
        $(window).resize(function () {
          setSize();
        });
      }
    })(jQuery);
  }
};
<h3>Puzzles</h3>
<table class="puzzles-widget">
  <tr>
    <td class="grid-column">
      <div class="grid-size-input">
        Grid Width: <input type="text" name="grid-width" value="7" />
        <a class="apply-grid">Apply</a>
      </div>

      <div class="grid-wrapper">
        <div class="grid">
          <?php foreach($referenced_entities as $key => $entity): ?>
            <div class="puzzle"
                 grid-width="<?php print $entity['grid_width']; ?>"
                 grid-height="<?php print $entity['grid_height']; ?>"
                 drupal-id="<?php print $entity['id']; ?>">
              <div class="title"><?php print $entity['title']; ?></div>
              <div class="number"><?php print $key; ?></div>
              <div class="remove">X</div>
              <div class="buttons">
                <a class="move-left"> < </a>
                <a class="move-right"> > </a>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </td>

    <td class="puzzles-area-column">
      <ul>
        <?php foreach($all_entities as $key => $entity): ?>
          <li>
            <a class="add-puzzle"
               grid-width="<?php print $entity['grid_width']; ?>"
               grid-height="<?php print $entity['grid_height']; ?>"
               title="<?php print $entity['title']; ?>"
               drupal-id="<?php print $entity['id']; ?>">
              Add "<?php print $entity['title']; ?>" Puzzle (<?php print $entity['grid_width']; ?>x<?php print $entity['grid_height']; ?>)
            </a>
          </li>
        <?php endforeach; ?>
      </ul>
    </td>
  </tr>
</table>